# This is auto-generated code from the source Pluto file.
from pluto_engine.language import *
from collections import OrderedDict


class Procedure_(Procedure):


    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.main_body.append(stmt_9cc11cdcb2c911edb6fbe4fd453300ee)


def stmt_9cc11cd8b2c911edbcace4fd453300ee(caller):
    caller.wait_for_relative_time(lambda x: ureg('2s'))


def stmt_9cc11cd9b2c911ed979de4fd453300ee(caller):
    caller.log(lambda x: "Hello from main")


def stmt_9cc11cdab2c911ed8d6ee4fd453300ee(caller):
    caller.wait_for_relative_time(lambda x: ureg('2s'))


def stmt_9cc11cdbb2c911ed8d21e4fd453300ee(caller):
    caller.log(lambda x: "Done with main")


def stmt_9cc11cdcb2c911edb6fbe4fd453300ee(caller):
    step = Step_stmt_9cc11cdcb2c911edb6fbe4fd453300ee(caller)
    continuation = OrderedDict()
    raise_event = None
    caller.initiate_and_confirm_step(step, continuation, raise_event)


class Step_stmt_9cc11cdcb2c911edb6fbe4fd453300ee(Step):

    def __init__(self, caller):
        super().__init__(caller)
        self.main_body.append(stmt_9cc11cd8b2c911edbcace4fd453300ee)
        self.main_body.append(stmt_9cc11cd9b2c911ed979de4fd453300ee)
        self.main_body.append(stmt_9cc11cdab2c911ed8d6ee4fd453300ee)
        self.main_body.append(stmt_9cc11cdbb2c911ed8d21e4fd453300ee)
